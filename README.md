This project is the implementation of a simulation for Consensus Network Intrusion Detection System (CNIDS) described in the paper "A consensus based network intrusion detection system" of Michel Toulouse, Bui Quang Minh and Philip Curtis.

Code author: Bui Quang Minh

Language: Java

Public Git repo: https://bitbucket.org/minhcly95/consensusnids.git

Usage: compile the project then launch using "java edu.vgu.nids.Program"

Warning: NSL-KDD data not included, please download it exclusively

Library: Jama and Weka library is available in lib/ folder

Package description:

	Program.java: main entry point, containing all configuration of the system

	core namespace: core and abstract classes of the system, including network, packet and report

	consensus namespace: classes for consensus calculation, including weight model, belief

	graph namespace: classes for graph representation, including graph architecture and models

	math namespace: classes for calculation beyond the range supported by Java

	input namespace: classes for input processing, including file reader and attribute filter

	output namespace: classes for log writing